<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Coppie Generator</title>
</head>

<body>
    <div style="display: flex; flex-flow:column wrap; align-items: center;">
        <h1>Wela ragazzi ma che ficata questo generatore di coppie</h1>
        <h2>Seleziona la tua hackademy:</h2>
        <form action="" method="post">
            <select
                style="padding: 8px; margin: 8px 8px; border-radius:12px; background-color: rgba(2,133,162,0.5); display:block;"
                name="hack" id="">
                <?php
                $hackademy = ['22','23'];
                echo "<option value=''>Nessuna scelta</option>";
                foreach ($hackademy as $hack) {
                    echo "<option value=\"$hack\">$hack</option>";
                }
            ?>
            </select>
            <input
                style="padding: 8px; margin: 8px 8px 8px 30px; border-radius:12px; background-color:rgba(253,246,227,1); display:block;"
                type="submit" name="submit" value="Conferma" />
        </form>
        <?php
    if (isset($_POST['submit']) && !empty($_POST['hack'])) {
        $selected_val = $_POST['hack'];
        echo "<p> Hai scelto la : $selected_val </p>";
        if ($selected_val == 23) {
            $students = [
                'Daniele Silvestrini',
                'Francesca Carrera',
                'Valentina Rappazzo',
                'Devy Cantalupo',
                'Vincenzo Pistoia',
                'Manuel Coluccia',
                'Antonio Capone',
                'Federico Fuso',
                'Igor Oliynyk',
                'Lorenzo Gobbo',
                'Lorenzo Giannone',
                'Claudio Navarrini',
                'Clarissa Lavino',
                'Danilo Rinaldi',
                'Rocco Dipaola',
                'Marco Duravia',
                'Rinaldi De Yehoshoua Mashiah',
                'Davide Galati',
                'Claudia Corotto',
                'Alfonso Scarfone',
                'Tiziana Torello',
                'Andrea Ricco',
                'Francesco Gentile',
                'Giulia Ferrarelli',
                'Martina Faccia',
                'Luis Vieira',
                'Ferdinando Garofalo',
                'Salvo Salerno'
            ];
        } elseif ($selected_val == 22) {
            $students = [
                'Aitel Nordin',
                'Armadhi Edlir',
                'Barbieri Francesco',
                'Bianchetti Alessandro',
                'Capomaccio Giovanni',
                'Catino Alberto',
                'Centrone Domenico',
                'Chiaradonna Domenico',
                'Cialdini Sara',
                'De Candia Leonardo',
                'Falcone Massimiliano',
                'Fattori Lorenzo',
                'Fiorilli Adriano',
                'Gentile Ettore',
                'Giacoppo Simone',
                'Latino Karin',
                'Lenti Francesco',
                'Logallo Valentina',
                'Lorusso Marianna',
                'Mirante Adriano',
                'Mosso Roberto',
                'Parlanti Enrico',
                'Salandini Andrea',
                'Scarlino Giuseppe',
                'Terazzi Simone',
                'Zerbo Michele',
                'Alessandro Giorgio',
            ];
        }
        $num_students = count($students);
        shuffle($students);
        shuffle($students);
        shuffle($students);
        echo "<div id='students_box'>";
        echo "<p style='text-align:center;'>Ciao Ragazzii ecco le coppie di oggi:</p> \n";
        for ($i = 0; $i < count($students); $i+=2) {
            $student1 = $students[$i];
            $student2 = $students[$i+1];
            if (count($students) %2 != 0 && $i+3 == count($students)) {
                $student1 = " " . $students[$i] . " e " . $students[$i + 1] . " e " . $students [$i + 2] . "in Aula studio " . $i+1;
                echo "<p> $student1 </p>";
                break;
            }
            echo " <p> $student1 e $student2 in Aula studio ". $i+1 . "</p>";
        }
        echo "</div>";
        echo"<button onclick='copyToClipboard()' style='padding: 8px; margin: 8px 8px 8px 30px; border-radius:12px; background-color:rgba(253,246,227,1); display:block; cursor:pointer;'>Copy text</button>";
        echo "<h3> Il totale degli studenti è $num_students </h3>";
    } else {
        echo "Non hai ancora scelto nulla! Prrr";
        exit;
    }
    ?>
    </div>
    <script>
        function copyToClipboard() {
            /* Get the text field */
            let copyText = document.getElementById("students_box");
            if (window.getSelection) {
                var range = document.createRange();
                range.selectNode(document.getElementById('students_box'));
                window.getSelection().removeAllRanges();
                window.getSelection().addRange(range);
                document.execCommand("copy");
            }
        }
    </script>
</body>

</html>